package br.inatel.eureka.In.IoT_Eureka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer	// Enable eureka server

public class Eureka extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Eureka.class);
	}
	public static void main(String[] args) {
		checkIfConfigFileExists();
		SpringApplication.run(Eureka.class, args);
	}
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "application.properties";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("# Give a name to the eureka server\n" + 
						"spring.application.name=In.IoT-eureka-server\n" + 
						"\n" + 
						"# default port for eureka server\n" + 
						"server.port=8761\n" + 
						"\n" + 
						"eureka.client.register-with-eureka=false\n" + 
						"eureka.client.fetch-registry=false\n" + 
						"eureka.client.serviceUrl.defaultZone=http://inIoTTest:lucasabbadeWare@localhost:8761/eureka/\n" + 
						"spring.security.user.name=inIoTTest\n" + 
						"spring.security.user.password=lucasabbadeWare\n");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/*
	@EnableWebSecurity
	static class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.csrf().disable().authorizeRequests()
	                .anyRequest()
	                .authenticated()
	                .and()
	                .httpBasic();
	    }
	}
	*/
	
}